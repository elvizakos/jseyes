(function () {

	/* "eye" object constructor */
	function eye ( el ) {

		eye.objectslist.push(this); /* Add object to objectslist */

		this.active = false;
		this.start = function () {

			if ( eye.listening === false ) {
				document.body.addEventListener ( 'mousemove' , watchMouse ) ;
				document.addEventListener ( 'mousemove' , watchMouse ) ;
				eye.listening = true;

				document.body.addEventListener( 'touchmove', watchTouches ) ;
				document.addEventListener( 'touchmove', watchTouches ) ;

			}

			this.active = true;
		}

		this.movePupil = function ( mx, my ) {
			/* let o = this.elements.container, ox=0, oy=0;
			   while ( o && o.nodeName != '#document' ) {
			   ox+=o.offsetLeft - o.scrollLeft;
			   oy+=o.offsetTop - o.scrollTop;
			   o=o.offsetParent;
			   }
			   ox+=50;
			   oy+=30; */
			let o = this.elements.container.getBoundingClientRect(); /* Get offset ( top , left ) of the container element  */
			let ox=o.left + 50;
			let oy=o.top + 30;
			let n = (Math.PI / 2 ) - Math.atan2 ( my - oy, mx - ox ); /* Get the angle of mouse in rads from the center of the container element */
			let ys2 = my < oy ? oy - my : my - oy; if ( ys2 > 15 ) ys2 = 15;
			let xs2 = mx < ox ? ox - mx : mx - ox; if ( xs2 > 40 ) xs2 = 40;

			this.elements.pupil.style.marginTop = (5 + (Math.cos(n) * ys2)) + 'px';
			this.elements.pupil.style.marginLeft = (25 + (Math.sin(n) * xs2)) + 'px';
		}

		this.elements = {};

		this.elements.container = (typeof el == 'object' && el instanceof HTMLElement ) ? el : document.createElement('eyes');
		
		this.elements.container.dataEyeObject = this;

		this.elements.pupil = document.createElement('div');
		this.elements.pupil.dataEyeObject = this;
		this.elements.container.appendChild( this.elements.pupil );

		this.start();

	}

	eye.objectslist = []; /* List of "eye" objects */
	eye.listening = false;

	eye.onload = function () {
		let el = document.getElementsByTagName('eyes');
		for ( let i = 0; i < el.length; i ++ )
			if ( typeof el[i].dataEyeObject == 'undefined' )
				(new eye ( el[i] ));
	};

	eye.loadApp = function ( app ) {
		var eye1 = new lvzwebdesktop.eye();
		var eye2 = new lvzwebdesktop.eye();
		var d = document.createElement('div');
		d.style.width = '204px';
		d.style.height = '62px';

		d.appendChild(eye1.elements.container);
		d.appendChild(eye2.elements.container);
		app.container.appendChild(d);

		var w = lvzwebdesktop(app.container);
		w.setWidth(204);
		w.setHeight(62);
		w.resizeOnContent(false);

		if ( w.type == 'window' ) {
			w.setLayout('menu:minimize,close');
			w.resizeOnContent(false);
			w.disableResize();
			w.props.disableBorder = true;
			w.props.resizable = false;
			w.props.maximizable = false;
		}
	};

	/* Watch for mouse movement inside document and run "movePupil" method for each active eye object */
	function watchMouse  ( e ) {
		for ( let i = 0; i < eye.objectslist.length; i ++ )
			if ( eye.objectslist[i].active === true )
				eye.objectslist[i].movePupil(e.clientX, e.clientY);
	}

	function watchTouches ( e ) {
		for ( let i = 0; i < eye.objectslist.length; i ++ )
			if ( eye.objectslist[i].active === true )
				eye.objectslist[i].movePupil(e.changedTouches[0].clientX, e.changedTouches[0].clientY);
	}

	if (document.readyState == 'complete') eye.onload();

	/* Make objects for each "eyes" tag when window's document is ready  */
	window.addEventListener('load', function () {
		eye.onload();
	});

	var url = new URL(document.currentScript.src);
	var c = url.searchParams.get("$");
	if ( c !== null ) window[c].eye = eye;
	else window.eye = eye;

})();
